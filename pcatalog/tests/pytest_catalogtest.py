import mock as mock
from mock import MagicMock
import pytest
from catalog.catalog import Catalog
from catalog.db import DB
from catalog.person import Person
from catalog.peselservice import PeselService


def person():
    return Person("name", "surname", "pesel")


def pesel_list():
    return [91070205895, 53012937512, 95042341234, 78111537449]


#1. pobranie PESEL po imieniu i nazwisku
@mock.patch('catalog.db.DB.get_pesel_list', MagicMock(return_value=pesel_list()))
def test_should_get_pesel_by_person():
    # given
    c = Catalog(DB, None)

    # when
    result = c.get_pesel("name", "surname")

    # then
    assert result == pesel_list()[0]
    DB.get_pesel_list.assert_called_with("name", "surname")


#2. pobranie listy PESEL po imieniu i nazwisku
@mock.patch('catalog.db.DB.get_pesel_list', MagicMock(return_value=pesel_list()))
def test_should_get_pesel_list_by_person():
    # given
    c = Catalog(DB, None)

    # when
    result = c.get_pesel_list("name", "surname")

    # then
    assert result == pesel_list()
    DB.get_pesel_list.assert_called_with("name", "surname")


#3. wyszukanie osoby po PESEL
@mock.patch('catalog.db.DB.get_person', MagicMock(return_value=person()))
def test_should_get_person_by_pesel():
    # given
    c = Catalog(DB, None)

    # when
    result = c.get_person("pesel")

    # then
    assert_equal_person(result, person())
    DB.get_person.assert_called_with("pesel")


def assert_equal_person(result, expected):
    assert result.get_name() == expected.get_name()
    assert result.get_surname() == expected.get_surname()
    assert result.get_pesel() == expected.get_pesel()


#4. dodanie osoby do katalogu - poprawny PESEL
@mock.patch('catalog.peselservice.PeselService.verify', MagicMock(return_value=True))
@mock.patch('catalog.db.DB.add_person', MagicMock(return_value=person()))
def test_should_add_person():
    # given
    c = Catalog(DB, PeselService)

    # when
    c.add_person("name", "surname", "91070205895")

    # then
    PeselService.verify.assert_called_with("91070205895")

    args = DB.add_person.call_args.args
    assert len(args) == 1
    assert args[0].__class__ == Person

    expected = Person ("name", "surname", "91070205895")

    assert_equal_person(args[0], expected)


#5. odmowa dodania osoby do katalogu - niepoprawny PESEL
@mock.patch('catalog.peselservice.PeselService.verify', MagicMock(return_value=False))
@mock.patch('catalog.db.DB.add_person', MagicMock())
def test_should_not_add_person():
    # given
    c = Catalog(DB, PeselService)

    # when
    with pytest.raises(Exception):
        c.add_person("name", "surname", "improper PESEL")

    # then
    PeselService.verify.assert_called_with("improper PESEL")

    DB.add_person.assert_not_called()