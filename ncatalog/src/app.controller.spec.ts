import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Repository } from './db/repository';
import { Catalog } from './service/catalog';
import { MockRepository, MockPESELService } from './service/mock-catalog';
import { Logger } from '@nestjs/common';
import { PESELService } from './service/pesel-service';

describe('AppController', () => {
    let appController: AppController;

    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            controllers: [AppController],
            providers: [
                Logger,
                {
                    provide: 'Repository',
                    useClass: MockRepository
                },
                {
                    provide: 'PESELService',
                    useClass: MockPESELService
                },
                Catalog
            ],
        }).compile();

        appController = app.get<AppController>(AppController);
    });

    describe('all', () => {
        it('should get mocked pesel', () => {
            expect(appController.getPesel("A", "B")).toBe('1111116');
        });

        it('should add person', () => {
            appController.addPerson("C", "D", "1111117");
            expect(appController.getPesel("C", "D")).toBe('1111117');
        });
    });
});

describe('Catalog', () => {

    let catalog: Catalog;
    let db: Repository;
    let peselService: PESELService;

    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            providers: [
                Catalog,
                {
                    provide: 'Repository',
                    useClass: MockRepoForTests
                },
                {
                    provide: 'PESELService',
                    useClass: MockPESELService
                }
            ],
        }).compile();

        catalog = app.get<Catalog>(Catalog);
    });

    it('getPesel', () => {
        expect(catalog.getPESEL('', '')).toBe('1111111');
    });

});

class MockRepoForTests implements Repository {
    getPESELList(name: string, surname: string): string[] {
        return ['1111111'];
    }
    getPerson(pesel: string): import("./service/person").Person {
        throw new Error("Method not implemented.");
    }
    insertPerson(person: import("./service/person").Person): void {
        throw new Error("Method not implemented.");
    }
}