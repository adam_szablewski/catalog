import { Person } from "src/service/person";

export interface Repository {
    getPESELList(name: string, surname: string): string[];

    getPerson(pesel: string): Person;

    insertPerson(person: Person): void;
}