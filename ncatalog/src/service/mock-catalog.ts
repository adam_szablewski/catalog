import { Injectable, Logger } from "@nestjs/common";
import { Repository } from "src/db/repository";
import { PESELService } from "./pesel-service";
import { Person } from "./person";

@Injectable()
export class MockRepository implements Repository {
    map: { [key: string]: Person } = { '1111116': new Person('A', 'B', '1111116') }

    getPESELList(name: string, surname: string): string[] {
        let result = Object.values(this.map)
            .filter(p => p.name === name && p.surname === surname)
            .map(p => p.pesel)
            ;

        return result;
    }
    getPerson(pesel: string): Person {
        return this.map[pesel];
    }
    insertPerson(person: Person): void {
        this.map[person.pesel] = person;
    }
}

@Injectable()
export class MockPESELService implements PESELService {
    verify(pesel: string): boolean {
        return true;
    }

}